//$window.sessionStorage.user = JSON.stringify(user);
//var user = JSON.parse($window.sessionStorage.user);
function loginWs(usuarioIn, passIn){
    var user = {
        usuario: usuarioIn,
        contrasena: passIn
    }
    var user = JSON.stringify(user); 
    $.ajax({
        url: 'http://localhost:8084/usuario/login',
        type: "POST",
        data: user,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if(data != null){
                if(data.id != null){
                    console.log('login true')
                    window.sessionStorage.user = JSON.stringify(data);
                    window.location.href = '/view/lista-proyectos.html';
                }else{
                    console.log('login false');
                }
                
            }else{
                console.log('login false');
            }
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function registrarProyectoWs(data){
    var data = JSON.stringify(data);
    $.ajax({
        url: 'http://localhost:8084/proyecto/save',
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Éxito',
                'Se registró el proyecto'
            ).then((result) => {
                reloadTableProyecto();
            });
        },
        error: function (error) {
            console.log(error);
            console.log(`Error ${error}`);
        }
    });
}

function searchUsarioByEmailWs(data, idUser){
    
    $.ajax({
        url: 'http://localhost:8084/usuario/getByEmail?email='+data+'&current='+idUser,
        type: "GET",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            fillTablSearchUser(data);
            return data;
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function getAllProyectosWs(idUser){
    $.ajax({
        url: 'http://localhost:8084/proyecto/getByUser?id='+idUser,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            fillTableListProyectos(data);
            return data;
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function deleteProyectoWs(idProyect){
    $.ajax({
        url: 'http://localhost:8084/proyecto/deleteById?id='+idProyect,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Eliminado',
                'Se eliminó el registro satisfactoriamente'
            ).then((result) => {
                reloadTableProyecto();
            });
            
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function loginGWs(nombre, email){
    var user = {
        nombre: nombre,
        email: email
    }
    var user = JSON.stringify(user); 
    $.ajax({
        url: 'http://localhost:8084/usuario/loginG',
        type: "POST",
        data: user,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if(data != null){
                if(data.email != null){
                    window.sessionStorage.user = JSON.stringify(data);
                    window.location.href = '/view/lista-proyectos.html';
                }else{
                    console.log('login false');
                }
                
            }else{
                console.log('login false');
            }
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function editProyectoWS(nombre, descripcion, id){
    var proyecto = {
        id: id,
        nombre: nombre,
        descripcion: descripcion
    }
    var proyecto = JSON.stringify(proyecto);
    $.ajax({
        url: 'http://localhost:8084/proyecto/update',
        type: "POST",
        data: proyecto,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Modificado',
                'Se modificó el registro satisfactoriamente'
            ).then((result) => {
                console.log('reload');
                reloadTableProyecto();
            });
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

//client id: 947699620439-f0h77pkjt32as9f0ub6f773lfkvfr1ch.apps.googleusercontent.com
//client secret: j15iVZ_hx5u7uC4AegNHVct5
