function registroProyecto (){   
    var nombreProyecto = $("#nombreProyecto").val();
    var descripcionProyecto = $("#descripProyecto").val();
    var usuarioXproyecto = [];
    var user = JSON.parse(window.sessionStorage.user);
    usuarioXproyecto.push({"id_usuario": + user.id });
    $('#tblUsuarioSeleccionados tr').each(function (i) {
        var value = $(this).attr('value');
        usuarioXproyecto.push({"id_usuario": + value });
    });
    var newProyecto = {
        "nombre":nombreProyecto,
        "descripcion": descripcionProyecto,
        "usuarioXproyecto":usuarioXproyecto
    }
    registrarProyectoWs(newProyecto);
}

function searchUsuarioProy (){
    var emailSearch = $("#txtSearchUsuario").val();
    var user = JSON.parse(window.sessionStorage.user);
    searchUsarioByEmailWs(emailSearch, user.id);
}

function fillTablSearchUser(data){
    clearTablSearchUser();
    for(let i=0; i<data.length;i++){
        var us = data[i].nombre+' '+data[i].apellidoPaterno
        var tr = '<tr value=' + data[i].id+'>';
        tr = tr.concat('<td>'+us+'</td>');
        tr = tr.concat('<td>'+data[i].email+'</td>');
        tr = tr.concat('</tr>');
        
        $("#tblUsuarioBuscados").find('tbody').append(tr);
    }
    //$("#listUsuarioProy").append("<li value='" + data.id + "'>"+us+"</li>");

}

function clearTablSearchUser(){
    $("#tblUsuarioBuscados tr").remove(); 
}

function clearTablSearchUserSeleccionado(){
    $("#tblUsuarioSeleccionados tr").remove(); 
}

function clearTablProyectos(){
    $("#listProyectos tr").remove(); 
}

function fillTableListProyectos(data){
    for(let i=0; i<data.length;i++){
        var tr = '<tr value=' + data[i].id+'>';
        tr = tr.concat('<td>'+data[i].nombre+'</td>');
        tr = tr.concat('<td>'+data[i].descripcion+'</td>');
        tr = tr.concat('<td><i class="editProy fas fa-edit"></i></td>');
        tr = tr.concat('<td><i class="remProy fas fa-trash-alt"></i></td>');
        tr = tr.concat('</tr>');
        
        $("#listProyectos").find('tbody').append(tr);
    }

}